<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reion Electric Luxury Bus</title>
     <?php include 'includes/styles.php'?>
</head>

<body>
    <?php include 'includes/header.php' ?>
    <!-- main -->
    <main class="subpage">
        <!-- sub page header -->
        <section class="subpageHeader">
             <div class="customContainer">
                 <div class="titleSection">
                     <div class="row">
                         <div class="col-md-6">
                             <div class="sectionTitle">
                                 <p>Reion Services</p>
                                 <h1 class="p-0 m-0">Our Services</h1>
                             </div>
                         </div>
                         <div class="col-md-6 align-self-center">
                              <ul class="nav justify-content-end">
                                   <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>                                   
                                   <li class="nav-item"><a class="nav-link" href="javascript:void(0)">Our Services</a></li>
                              </ul>
                         </div>
                     </div>
                 </div>
             </div>
        </section>
        <!--/ sub page header -->
        <!-- sub page body -->
        <section class="subpageBody">
             <!-- container -->
             <div class="customContainer">                
                <div class="servicesbg bgSection">
                    <div class="container text-center">
                        <h6>Reion Services</h6>
                        <h5>Reion motor  is your trusted partner for the purchase of new buses and coaches. At reion motor, you’ve come to the right address for used vehicles. Through reion mototrs Services, we offer a wide range of services tailored to your individual needs. From the supply of spare parts to 24-hour service; from repair and maintenance to training. </h5>
                        <a class="btn redbtn" href="contact.php">Reach us</a>
                    </div>
                </div>
                 <!-- row -->
                 <div class="sectionMargin">
                    <div class="row ">
                        <!-- col -->
                        <div class="col-md-5">
                            <img src="img/servicesimg.jpg" alt="" class="img-fluid w-100">
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-md-7 align-self-center">
                            <!-- accordion -->
                            <div class="accordion customAccordion" id="accordionExample">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Keep your bus uptodate
                                    </button>
                                    </h2>
                                    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                         <p>A service contract has one purpose only: to keep your vehicles available, safe and productive. A well-maintained bus or coach will simply do a better job. The risk of unplanned stops, or even breakdowns, is significantly reduced. What’s more, a properly serviced vehicle is also better at maintaining its value.</p>
                                         <img src="img/service01.jpg" alt="" class="img-fluid w-100">
                                    </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                       Battery Contract
                                    </button>
                                    </h2>
                                    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <p>The Battery Contract is an assurance of battery performance at a specified monthly cost. reion Bus monitors the battery system of each vehicle to secure performance and trouble-free operation. This follow-up is done by remote-access data capture and does not require any action by the operator. So you don’t have to worry about battery capacity or the need for battery replacement.</p>
                                    </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingThree">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Your reion stays as good as new
                                    </button>
                                    </h2>
                                    <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                       <p>After many years of service, wear and tear will inevitably show. But under the skin a reion is close to immortal. That’s why the life of our vehicles can be profitably extended by several years through a premium refurbishment programme. New flooring, paint, textiles and seats are part of the overhaul that will turn your trusty old bus or coach into a money-maker.</p>
                                    </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingFour">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        Geniuin spare parts 
                                    </button>
                                    </h2>
                                    <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                           <p>A Genuine reion Part has exactly the same kind of quality as the original part fitted when the bus was built. Specifically developed to deliver long product life with full warranty, minimising trouble and ensuring maximum uptime.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/ accordion -->
                        </div>
                        <!-- col -->
                    </div>
                 </div>                 
                 <!--/ row -->

                 <div class="row pb-3 pb-lg-5">
                     <div class="col-md-5 order-md-last">
                         <img src="img/busservice.jpg" alt="" class="img-fluid w-100">
                     </div>
                     <div class="col-md-7 align-self-center">
                         <h4 class="subTitle">AMC for Electric Vehicles</h4>
                         <p>The Service and Maintenance of an automobile guarantees the flawless functioning of the vehicle. Making sure that the vehicle is ready when ever required is prime. To make this  possible, world over the fleet operators invest in there own work shops and service teams. They stock spare parts and run almost a parallel business line which takes care of the fleet. This has put many of them concentrate more on the vehicle maintenance rather then their core business.</p>

                         <p> trend is slowly moving away, just as in many other products lines, to out  source the maintenance. We believe that the best way to maintain the vehicles is to have a own workshop so that the vehicles get the best of attention from the owner. This is good if the business for which the vehicles have been purchased does not require much of the fleet operators attention. This is not true in most other cases. To support the fleet operator focus on their core business we have the following options for the maintenance of the vehicles.></p>

                         <a href="downloads/AMCforElectricVehicles.pdf" download class="btn redbtn">Download More Info</a>
                     </div>
                 </div>
                
             </div>
             <!--/ container -->
        </section>
        <!--/ sub page body -->
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?>
</body>

</html>