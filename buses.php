<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reion Electric Luxury Bus</title>
     <?php include 'includes/styles.php'?>
</head>

<body>
    <?php include 'includes/header.php' ?>
    <!-- main -->
    <main class="subpage">
        <!-- sub page header -->
        <section class="subpageHeader">
             <div class="customContainer">
                 <div class="titleSection">
                     <div class="row">
                         <div class="col-md-6">
                             <div class="sectionTitle">
                                 <p>Buses</p>
                                 <h1 class="p-0 m-0">City Link Luxury & Standard</h1>
                             </div>
                         </div>
                         <div class="col-md-6 align-self-center">
                              <ul class="nav justify-content-end">
                                   <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>                                   
                                   <li class="nav-item"><a class="nav-link" href="javascript:void(0)">City Link Luxury & Standard</a></li>                                   
                              </ul>
                         </div>
                     </div>
                 </div>
             </div>
        </section>
        <!--/ sub page header -->
        <!-- sub page body -->
        <section class="subpageBody">
             <!-- container -->
             <div class="customContainer">   
                 
                <!-- row -->
                <div class="row py-3 pylg-5">
                    <div class="col-md-6">
                        <img src="img/busluxury.jpg" alt="" class="img-fluid w-100">
                    </div>
                    <div class="col-md-6 align-self-center">
                        <div class="sectionTitle">
                            <p>Buses</p>
                            <h3>City Link Luxury</h3>                        
                        </div>
                        <p>With the Reion 1200L Electric bus range you get clean, silent and efficient public transport, a new level of capacity, and our flexible charging concept ensures operation on your current schedule.  With our electric buses, services and Reion as your partner, your city can take the next step in the transformation towards a Zero City. </p>
                        <p>The RE 1200L Its intelligent interaction between innovative battery and charging technology, networking of IT and communication systems, and not least its striking futuristic design make it the new standard for electric city buses, ready today for the city of tomorrow</p>
                         <a href="downloads/luxury.pdf" download class="btn redbtn">Download Brochure</a>
                    </div>
                </div>
                <!--/ row -->

                 <!-- row -->
                <div class="row py-3 pylg-5">
                    <div class="col-md-6 order-lg-last">
                        <img src="img/busstandard.jpg" alt="" class="img-fluid w-100">
                    </div>
                    <div class="col-md-6 align-self-center">
                        <div class="sectionTitle">
                            <p>Buses</p>
                            <h3>City Link Standard</h3>                        
                        </div>
                        <p>Exceptionally quiet, emission-free, distinguished by its modern design and guaranteeing low operation costs - discover the benefits of the Reion re1200 electric.  Thanks to innovative technical solutions electric buses can operate for an unlimited time, up to 24 hours a day.  Moreover, their low noise emission and vibration level makes battery buses particularly suited for use in city centres. </p>
                        <p>The vehicle is available in both a low-floor and low-entry varients .</p>
                        <a href="downloads/Standard.pdf" download class="btn redbtn">Download Brochure</a>
                    </div>
                </div>
                <!--/ row -->

                 <!-- row -->
                <div class="row py-3 pylg-5">                    
                    <div class="col-md-12 align-self-center">
                        <div class="sectionTitle">
                            <p>Buses</p>
                            <h3>Your Partner to Drive Profibility</h3>                        
                        </div>
                        <p>REION has an electric e-bus production plant in Hyderabad,  India, with an annual production capacity of over 1000 e-buses.  Furthermore, REION designed and built and e-bus for BHEL, which is marketed as a BHEL bus.  Each of these buses are air-conditioned and also has air suspension.  With the exception of the motor, ancillary components, and batteries, each device is designed in-house and manufactured in India.  REION e-bus will be suitable for delivering low-cost transportation to the general public throughout municipalities.  These buses can also be utilized as intercity buses throughout the day.</p>

                        <p>Reion has designed a number of different settings, such as ordinary and semi-luxury seating arrangements, to provide for a variety of diverse applications, including ambulances and other unique designs, such as emergency vehicles and mobile medical operating theatres.  Reion's experience extends beyond bus manufacture to the creation of the necessary infrastructure for charging stations at depots for the relevant municipality, as well as their maintenance under agreed-upon conditions. </p>
                        <p>Details, including costs, are supplied in an addendum to this instructive leaflet, along with crucial technical information.  It is our responsibility to notify you and respond to any extra requirements your company may have that we may accomodate into our design.</p>
                    </div>
                </div>
                <!--/ row -->

                  <!-- row -->
                <div class="row py-3 pylg-5">     
                    <div class="col-md-6">
                        <img src="img/passengersafety.jpg" alt="" class="img-fluid w-100">
                    </div>               
                    <div class="col-md-6 align-self-center">
                        <div class="sectionTitle">
                            <p>Buses</p>
                            <h3>Passenger Saefty</h3>                        
                        </div>                       
                        <p>Passenger safety is the prime focus when it comes to the design of our buses.  The Monocoque design makes sure that in case of an head an collision, the passenger compartment does not collapse there by protecting the passengers from getting crushed.  Every element of the structure is designed and fabricated with the best of material adhering to the stringent quality standards.  the welding is done by certified welders on jigs and fixtures. </p>

                        <p>The roll over tests conform the ability of the structure to withstand the impact loads that tend to crush the structure in the vent of a toppling of the bus.  the Center of Gravity is kept a a low point to make sure that the stablity of the bus is very high in bad road condition and also during any side impact of collision. </p>
                    </div>
                    
                </div>
                <!--/ row -->

                 <!-- row -->
                <div class="row py-3 pylg-5">    
                    <div class="col-md-6 order-md-last align-self-center">
                        <img src="img/qualitycontrol.jpg" alt="" class="img-fluid w-100">
                    </div>                     
                    <div class="col-md-6 align-self-center">
                        <div class="sectionTitle">
                            <p>Buses</p>
                            <h3>Quality control</h3>                        
                        </div>                                          
                        <p>We use the best of equipment in the industry and trained manpower the bus is 'sculpted' to perfection in work factory.  The supervisors and the workmen are picked up after elaborate testing procedure and then trained before they are allowed to work on the production.  The drive to produce a bus a passion.  The passion to make sure that the product run with zero defect. The best of the aggregates are used when it comes to the rear axle.  Brakes, suspension and air compressor.  The components are procured from top companies like,  WABCO, Merritor, Haritasa, TVS etc </p>

                        <p>ESP intervenes to restore directional stability when there is a difference between the vehicle's actual movement.  During "under-steer", i.e when the vehicle travels on a greater radius than that defined by the steering wheel angle, the vehicle is turned back onto the 'correct' course by ESP applying the brakes at the inside rear wheel. </p>
                    </div>
                     
                </div>
                <!--/ row -->
               
             </div>
             <!--/ container -->
        </section>
        <!--/ sub page body -->
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?>
</body>

</html>