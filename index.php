<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reion Electric Luxury Bus</title>
     <?php include 'includes/styles.php'?>
     <?php include 'includes/data.php'?>
</head>

<body>
    <?php include 'includes/header.php' ?>
    <!-- main -->
    <main>
        <!--slider -->
        <section class="homeSlider">
            <!-- Swiper -->
            <div class="swiper-container homeinnerSlider">
                <div class="swiper-wrapper">

                <?php 
                for ($i=0; $i<count($homeSliderItem); $i++) { ?>
                    <div class="swiper-slide" style="background-image:url(./img/<?php echo $homeSliderItem[$i][0]?>)">
                        <!--contnet -->
                        <article>
                            <h2><?php echo $homeSliderItem[$i][1]?> <span class="fbold"><?php echo $homeSliderItem[$i][2]?></span></h2>
                            <p><?php echo $homeSliderItem[$i][3]?></p>
                            <a href="<?php echo $homeSliderItem[$i][4]?>" class="btn">Read More</a>
                        </article>
                        <!--/ content -->
                    </div>
                    <?php } ?>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination swiper-pagination-white"></div>
            </div>
        </section>
        <!--/ slider -->
        <!-- home about -->
        <section class="homeAbout">
            <div class="customContainer">
                <!-- row -->
                <div class="row">                   
                    <div class="col-md-6 align-self-center position-relative">
                        <h2 class="strokeFont">Company</h2>
                        <div class="sectionTitle">
                            <p>About Reion</p>
                            <h3>E-Bus Engineering the Best Solutions</h3>
                            <h6>Premium passenger experience</h6>
                        </div>
                        <p>Safety, comfort and reliability. Our products and services are designed for a first-class
                            passenger experience, ranging from efficient city transport solutions to luxury coach
                            travel. We aim to deliver a premium experience every time.</p>
                        </p>
                        <a class="btn redbtn" href="company.php">Read More</a>
                    </div>
                     <div class="col-md-6">
                       <img src="img/aboutimghome.png" alt="" class="img-fluid">
                    </div>
                  
                </div>
                <!--/ row -->
            </div>
        </section>
        <!--/ homeabout -->
        <!-- Home Vehicles-->
        <section class="homeVehicles">
            <!-- container -->
            <div class="container-fluid gx-0">
                <div class="swiper-container homeVehicles">
                    <div class="swiper-wrapper">
                        <?php 
                        for($i=0; $i<count($homeVehicleItem); $i++) { ?> 
                        <div class="swiper-slide">
                            <img src="img/homevehicle/<?php echo $homeVehicleItem [$i][0]?>" alt="" class="img-fluid">
                            <article>
                                <h4 class="ps-2"><?php echo $homeVehicleItem [$i][1]?></h4>
                                <div class="d-flex vehicleDescParent">
                                    <div class="vehicleDesc">
                                        <h5 class="fsbold"><?php echo $homeVehicleItem [$i][1]?></h5>
                                        <p><?php echo $homeVehicleItem [$i][2]?></p>
                                    </div>
                                    <a href="<?php echo $homeVehicleItem [$i][3]?>" class="descLink"><span
                                            class="icon-rightchervon icomoon"></span></a>
                                </div>
                            </article>
                        </div>
                      <?php }  ?>
                    </div>                  
                    <!-- Add Arrows -->
                    <div class="swiper-button-next swiper-button-white"></div>
                    <div class="swiper-button-prev swiper-button-white"></div>
                </div>
            </div>
            <!--/ container -->
        </section>
        <!--/ Home Vehicles -->
        <!-- cards -->
        <section class="homeCards sectionMargin">
            <div class="customContainer">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-4">
                        <div class="cardCol">
                            <div class="icon">
                                <span class="icon-spareparts icomoon"></span>
                            </div>
                            <article class="p-2 p-md-5">
                                <h5 class="fbold">Geniuin Spare Parts</h5>
                                <p>A Genuine reion Part has exactly the same kind of quality as the original part fitted
                                    when the bus was built. Specifically developed to deliver long product life with
                                    full warranty, minimising trouble and ensuring maximum uptime.</p>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-4">
                        <div class="cardCol">
                            <div class="icon">
                                <span class="icon-remote icomoon"></span>
                            </div>
                            <article class="p-2 p-md-5">
                                <h5 class="fbold">Remote health check</h5>
                                <p>Close follow-up of your vehicles condition can save a lot of money. Our Vehicle
                                    Status service lets you identify upcoming issues before they turn into problems that
                                    may lead to standstills and unplanned repair costs.</p>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-4">
                        <div class="cardCol">
                            <div class="icon">
                                <span class="icon-trackingbus icomoon"></span>
                            </div>
                            <article class="p-2 p-md-5">
                                <h5 class="fbold">Keep track and optimize</h5>
                                <p>Reion Fleet Management keeps you in control at all times – so you know the exact
                                    status of your operation, the health of your fleet, when unplanned stops occur and
                                    how your drivers are performing.</p>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
        </section>
        <!--/ cards -->
        <!-- technologies-->
        <section class="homeTechnologies">
            <div class="customContainer">
                <div class="sectionTitle border-bottom mb-4 pb-2">
                    <p>Technology</p>
                    <h3>Reion E-Bus Technology</h3>
                </div>
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-6 col-lg-3">
                        <div class="techCol">
                            <img src="img/tech01.jpg" alt="" class="img-fluid w-100">
                            <article class="py-3">
                                <h5 class="h5 fbold">Designed in house to suit the customer requirement</h5>
                                <p>Our buses are built on a complete monocoque structure enabling them for a steady
                                    motion supported by high performance shock absoring suspension system linked with
                                    hydraulic steering.</p>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-6 col-lg-3">
                        <div class="techCol">
                            <img src="img/tech02.jpg" alt="" class="img-fluid w-100">
                            <article class="py-3">
                                <h5 class="h5 fbold">Structuring</h5>
                                <p>Built on a Monocoque with a tubular structure.</p>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-6 col-lg-3">
                        <div class="techCol">
                            <img src="img/tech03.jpg" alt="" class="img-fluid w-100">
                            <article class="py-3">
                                <h5 class="h5 fbold">Aggregates</h5>
                                <p>The REION buses are built with a sound vision of existence and penetration. To
                                    deliver the best of the product to the end users and its commuters REION not
                                    compromising has partnered with the best & established providers of the aggregates.
                                </p>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-6 col-lg-3">
                        <div class="techCol">
                            <img src="img/tech04.jpg" alt="" class="img-fluid w-100">
                            <article class="py-3">
                                <h5 class="h5 fbold">Operating System</h5>
                                <p>Sophisticatedly built & placement of components makes it easy to comfortably assist
                                    in operating the vehicle with easy access and maintenance.</p>
                            </article>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
        </section>
        <!-- technologies-->
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?>
</body>

</html>