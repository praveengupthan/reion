<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reion Electric Luxury Bus</title>
     <?php include 'includes/styles.php'?>
</head>

<body>
    <?php include 'includes/header.php' ?>
    <!-- main -->
    <main class="subpage">
        <!-- sub page header -->
        <section class="subpageHeader">
             <div class="customContainer">
                 <div class="titleSection">
                     <div class="row">
                         <div class="col-md-6">
                             <div class="sectionTitle">
                                 <p>About Reion</p>
                                 <h1 class="p-0 m-0">Company</h1>
                             </div>
                         </div>
                         <div class="col-md-6 align-self-center">
                              <ul class="nav justify-content-end">
                                   <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                                   <li class="nav-item"><a class="nav-link" href="javascript:void(0)">About</a></li>
                                   <li class="nav-item"><a class="nav-link" href="javascript:void(0)">Company</a></li>
                              </ul>
                         </div>
                     </div>
                 </div>
             </div>
        </section>
        <!--/ sub page header -->
        <!-- sub page body -->
        <section class="subpageBody">
             <!-- container -->
             <div class="customContainer">
                 <!--row -->
                 <div class="row">
                     <!-- col -->
                     <div class="col-lg-4">
                         <div class="highletecol">
                             <h2>99%</h2>
                             <h3 class="fbold">Project Success</h3>
                             <p>Taking seamless key performance indicators.</p>
                         </div>
                     </div>
                     <!--/ col -->
                     <!-- col -->
                     <div class="col-lg-4">
                         <div class="highletecol">
                             <h2>100+</h2>
                             <h3 class="fbold">Employees</h3>
                             <p>Taking seamless key performance indicators.</p>
                         </div>
                     </div>
                     <!--/ col -->
                     <!-- col -->
                     <div class="col-lg-4">
                         <div class="highletecol">
                             <h2>50+</h2>
                             <h3 class="fbold">World wide Clients</h3>
                             <p>Taking seamless key performance indicators.</p>
                         </div>
                     </div>
                     <!--/ col -->
                 </div>
                 <!--/ row -->

                 <!-- row -->
                 <div class="sectionMargin">
                    <div class="row ">
                        <!-- col -->
                        <div class="col-md-4 align-self-center">
                            <div class="sectionTitle">
                                <p>Welcome to Reion</p>
                                <h3>This is Reion Buses</h3>                                
                            </div>
                            <p>Reion motors is one of the world’s leading providers of sustainable people transport solutions. We offer premium city- and intercity buses, coaches, and bus chassis as well as a wide range of services for increased productivity, uptime and safety.</p>
                            <h4 class="subTitle">Premium passenger experience</h4>
                            <p>Safety, comfort and reliability. Our products and services are designed for a first-class passenger experience, ranging from efficient city transport solutions to luxury coach travel. We aim to deliver a premium experience every time.    </p>
                            <p>Our focus on connecting aspirations and our pipeline of tech-enabled products keeps us at the forefront of the market.</p>
                        </div>
                        <!-- col -->
                         <!-- col -->
                        <div class="col-md-4 align-self-center">                          
                            <p> We have identified six key mobility drivers that will lead us into the future – modular architecture, complexity reduction in manufacturing, connected & autonomous vehicles, clean drivelines, shared mobility, and low total cost of ownership. Our brand REION is an incubating centre of innovation that will spark new mobility solutions through new technologies, business models and partnerships.</p>
                           
                            <p>As we continue to evolve and embrace change, there is one thing that has and will always remain the same – our focus on Leadership, Excellence, Agility and Performance. We Lead by example and strive to deliver Excellence across our businesses  worldwide. We provide an Agile and dynamic environment that enables  to maximize Performance and value for all stakeholders.</p>
                        </div>
                        <!-- col -->
                        <!-- col -->
                        <div class="col-md-4">
                            <img src="img/company2.jpg" alt="" class="img-fluid w-100">
                        </div>
                        <!--/ col -->
                    </div>
                 </div>                 
                 <!--/ row -->
                 <!-- row -->
                 <div class="row">
                    <div class="col-md-6 align-self-center">
                        <div class="sectionTitle">
                            <p>Mission</p>
                            <h3>Mission & Vision</h3>                                
                        </div>
                        <p>We innovate mobility solutions with passion to enhance the quality of life</p>
                        <p>By FY 2024, we will become the most aspirational Indian auto brand, consistently winning, by</p>
                        <ul class="listItems">
                            <li>Delivering superior financial returns</li>
                            <li>Driving sustainable mobility solutions</li>
                            <li>Exceeding customer expectations, and</li>
                            <li>Creating a highly engaged work force</li>
                        </ul>
                     </div>
                     <div class="col-md-6">
                         <img src="img/aboutmission.jpg" alt="" class="img-fluid w-100">
                     </div>
                 </div>
                 <!--/ row -->

                 <!-- our philosphy row -->
                 <div class="sectionMargin">
                    <div class="sectionTitle border-bottom">
                        <p>Our Philosphy</p>
                        <h3>Culture Pillars</h3>                                
                    </div>
                    <div class="row pt-4">
                        <!-- col -->
                        <div class="col-md-3">
                            <img src="img/culture01.jpg" alt="" class="img-fluid w-100">
                            <article class="pt-3">
                                <h5 class="fbold">Be Bold</h5>
                                <p>Distinctively exploit optimal alignments for intuitive bandwidth. </p>
                                <ul class="listItems">
                                    <li>Agility</li>
                                    <li>Risk Taking</li>
                                </ul>
                            </article>
                        </div>  
                        <!--/ col -->
                          <!-- col -->
                        <div class="col-md-3">
                            <img src="img/culture02.jpg" alt="" class="img-fluid w-100">
                            <article class="pt-3">
                                <h5 class="fbold">Own IT</h5>                               
                                <ul class="listItems">
                                    <li>Empowerment</li>
                                    <li>Owner's Mindset</li>
                                </ul>
                            </article>
                        </div>  
                        <!--/ col -->
                          <!-- col -->
                        <div class="col-md-3">
                            <img src="img/culture03.jpg" alt="" class="img-fluid w-100">
                            <article class="pt-3">
                                <h5 class="fbold">Solve Together</h5>
                               
                                <ul class="listItems">
                                    <li>Collaberation</li>                                    
                                </ul>
                            </article>
                        </div>  
                        <!--/ col -->
                          <!-- col -->
                        <div class="col-md-3">
                            <img src="img/culture04.jpg" alt="" class="img-fluid w-100">
                            <article class="pt-3">
                                <h5 class="fbold">Be Empathic</h5>
                              
                                <ul class="listItems">
                                    <li>Embracing Diversity</li>
                                    <li>Passion for Customers</li>
                                </ul>
                            </article>
                        </div>  
                        <!--/ col -->
                    </div>
                </div>
                 <!--/ our philosphy row -->
             </div>
             <!--/ container -->
        </section>
        <!--/ sub page body -->
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?>
</body>

</html>