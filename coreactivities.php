<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reion Electric Luxury Bus</title>
     <?php include 'includes/styles.php'?>
</head>

<body>
    <?php include 'includes/header.php' ?>
    <!-- main -->
    <main class="subpage">
        <!-- sub page header -->
        <section class="subpageHeader">
             <div class="customContainer">
                 <div class="titleSection">
                     <div class="row">
                         <div class="col-md-6">
                             <div class="sectionTitle">
                                 <p>About Reion</p>
                                 <h1 class="p-0 m-0">Core Activities</h1>
                             </div>
                         </div>
                         <div class="col-md-6 align-self-center">
                              <ul class="nav justify-content-end">
                                   <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>
                                   <li class="nav-item"><a class="nav-link" href="javascript:void(0)">About</a></li>
                                   <li class="nav-item"><a class="nav-link" href="javascript:void(0)">Core Activities</a></li>
                              </ul>
                         </div>
                     </div>
                 </div>
             </div>
        </section>
        <!--/ sub page header -->
        <!-- sub page body -->
        <section class="subpageBody">
             <!-- container -->
             <div class="customContainer">                
                <div class="coreActivities bgSection">
                    <div class="container text-center">
                        <h6>Our Quality</h6>
                        <h5>A team of highly skilled manufacturers, prepared to meet any request for fabrication, tool crafting or product design and production. </h5>
                        <a class="btn redbtn" href="contact.php">Reach us</a>
                    </div>
                </div>

                 <!-- row -->
                 <div class="sectionMargin">
                    <div class="row ">
                        <!-- col -->
                        <div class="col-md-6">
                            <img src="img/coreactivities01.jpg" alt="" class="img-fluid w-100">
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-md-6 align-self-center">
                            <div class="sectionTitle">
                                <p>Activities</p>
                                <h3>Our Activities</h3>                                
                            </div>
                            <p>The core activities of REION GROUP  consist of the development, manufacturing, sales and after-sales of a wide range of buses and coaches, the conversion or extension of mini & mid buses  reion motors  places high value on quality, safety, durability, the environment, low fuel consumption, comfort and low maintenance costs.      In the transition to zero emission transport, reion motors  offers  solutions and is not only bus supplier but also system supplier.   Sales of reion motors products take place through a worldwide network consisting of corporate-owned sales offices</p>                            
                            <p>For after-sales and maintenance, the customer can count on rapid, hassle-free assistance from reion motors employees in any of the many service locations. An extensive distribution network ensures that spare parts and accessories are delivered to the requested destination as quickly as possible. Reion motors is one of the largest bus and coach manufacturer  in india </p>
                            <div class="row">
                                <div class="col-md-6">
                                     <h4 class="subTitle">Future-oriented</h4>
                                     <p>… is our vision on sustainability, the environment, digital developments and new passenger transport initiatives. It is also about how we do more than delivering buses alone. We see what the world wants and can adapt and be inventive to meet these new trends.</p>
                                </div>
                                 <div class="col-md-6">
                                     <h4 class="subTitle">Reliable</h4>
                                     <p>… is about how we can act as a partner together with our stakeholders and help them in a fast-changing world with many new developments. But it’s also about the undisputed quality that we deliver in answering every demand.</p>
                                </div>
                                <div class="col-md-12">
                                     <h4 class="subTitle">Flexible</h4>
                                     <p>… is about the fact that every situation and demand is different and the way we deal with this. The uniqueness and strength in tailor-made solutions is how we offer flexible solutions for specific situations, rather than standard options.</p>
                                </div>
                            </div>
                        </div>
                        <!-- col -->
                    </div>
                 </div>                 
                 <!--/ row -->
                 <!-- row -->
                 <div class="row pb-5">
                    <div class="col-md-6 align-self-center">
                        <div class="sectionTitle">
                            <p>About Reion</p>
                            <h3>Code of Conduct</h3>                                
                        </div>
                        <p>REION group is one of the leading groups in indian market with  an open corporate culture and short lines of communication contribute to exceptional decisiveness. Our approach is to always be forward-looking and innovative in finding and implementing even more environmentally friendly and efficient solutions. Solutions that go beyond just supplying buses and coaches.   To achieve this, we think differently. And we take a different approach. We have built up a reputation as a brand over a long period of time. This also means that we set the bar high and set high standards for ourselves. </p>
                        <p>Our Code of Conduct describes what we expect of our employees, how we live up to our corporate values, how we do business and what our responsibility is with regard to environment and society.</p>                      
                     </div>
                     <div class="col-md-6">
                         <img src="img/codeofconduct.jpg" alt="" class="img-fluid w-100">
                     </div>
                 </div>
                 <!--/ row -->
             </div>
             <!--/ container -->
        </section>
        <!--/ sub page body -->
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?>
</body>

</html>