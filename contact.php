<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reion Electric Luxury Bus</title>
     <?php include 'includes/styles.php'?>
</head>

<body>
    <?php include 'includes/header.php' ?>
    <!-- main -->
    <main class="subpage">
        <!-- sub page header -->
        <section class="subpageHeader">
             <div class="customContainer">
                 <div class="titleSection">
                     <div class="row">
                         <div class="col-md-6">
                             <div class="sectionTitle">
                                 <p>Reach us</p>
                                 <h1 class="p-0 m-0">Contact</h1>
                             </div>
                         </div>
                         <div class="col-md-6 align-self-center">
                              <ul class="nav justify-content-end">
                                   <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>  
                                   <li class="nav-item"><a class="nav-link" href="javascript:void(0)">Contact</a></li>
                              </ul>
                         </div>
                     </div>
                 </div>
             </div>
        </section>
        <!--/ sub page header -->
        <!-- sub page body -->
        <section class="subpageBody">
             <!-- container -->
             <div class="customContainer">   
                 
                 <div class="sectionTitle">    
                     <p>Have a Question?</p>               
                    <h3>Let's Get in touch</h3>                   
                 </div>

                 <?php     

if(isset($_POST['submitContact'])){
$to = "info@reiongroup.com"; 
$subject = "Mail From ".$_POST['name'];
$message = "
<html>
<head>
<title>HTML email</title>
</head>
<body>
<p>".$_POST['name']." has sent mail!</p>
<table>
<tr>
<th align='left'>Name</th>
<td>".$_POST['name']."</td>
</tr>
<tr>
<th align='left'>Contact Number</th>
<td>".$_POST['phone']."</td>
</tr>
<tr>
<th align='left'>Email</th>
<td>".$_POST['email']."</td>
</tr>
<tr>
<th align='left'>Location</th>
<td>".$_POST['location']."</td>
</tr>
<th align='left'>Subject</th>
<td>".$_POST['subject']."</td>
</tr>
<tr>
<th align='left'>Message</th>
<td>".$_POST['msg']."</td>
</tr>
</table>
</body>
</html>
";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From:' .$_POST['name']. "\r\n";
//$headers .= 'Cc: myboss@example.com' . "\r\n";

mail($to,$subject,$message,$headers);   

//success mesage
?>
<div class="alert alert-success alert-dismissible fade show" role="alert">
  Mail Sent Successfully. Thank you <?= $_POST['name'] ?>, we will contact you shortly.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<?php

// echo "<p style='color:green'>Mail Sent. Thank you " . $_POST['name'] . ", we will contact you shortly.</p>";
}
?>

                 <!-- form -->
                 <form class="my-5 form" id="contactForm" method="post" action="">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-floating mb-4">
                                <input type="text" class="form-control" id="name" placeholder="Write Your Name" name="name">
                                <label for="name">Write Your Name</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-floating mb-4">
                                <input type="text" class="form-control" id="email" placeholder="Write Email Address" name="email">
                                <label for="email">Write Email Address</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-floating mb-4">
                                <input type="text" class="form-control" id="phone" placeholder="Write Phone Number" name="phone">
                                <label for="phone">Write Phone Number</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-floating mb-4">
                                <input type="text" class="form-control" id="location" placeholder="Ex: Hyderabad" name="location">
                                <label for="location">Location Ex:Hyderabad</label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-floating mb-4">
                                <input type="text" class="form-control" id="subject" placeholder="Subject" name="subject">
                                <label for="subject">Subject</label>
                            </div>
                        </div>
                          <div class="col-md-12">
                            <div class="form-floating mb-4">
                               <textarea style="height:100px;" class="form-control" placeholder="Leave a comment here" id="describe" name="msg"></textarea>
                                <label for="describe">Describe your Request</label>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <button class="btn redbtn" name="submitContact">Submit Your Query</button>
                        </div>
                    </div>
                 </form>
                 <!--/ form -->

                 <!-- address row -->
                 <div class="addressrow row">
                    <!-- address col -->
                    <div class="col-md-6">
                        <h4 class="subTitle">Registered Office</h4>
                            <p class="fsbold">Reion Automobiles Private India Limited,</p>
                        <p>Plot No. 151, 3rd Floor, Block No. 10, Jayabheri Park, Komaplly, Hyderabad, Telangana – 500014</p>

                        <article>
                            <small style="color:gray">Phone Number</small>
                            <p>414-214-0362, 515-215-0363</p>
                        </article>
                            <article>
                            <small style="color:gray">Email us</small>
                            <p>info@reiongroup.com/p>
                        </article>
                    </div>
                    <!--/ address col -->

                    <!-- map -->
                    <div class="col-md-6">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3804.4552147978416!2d78.47950841487837!3d17.533500687991182!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd79a06876d8f266!2zMTfCsDMyJzAwLjYiTiA3OMKwMjgnNTQuMSJF!5e0!3m2!1sen!2sin!4v1646846670714!5m2!1sen!2sin" width="100%" height="350" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                    <!--/ map -->
                 </div>
                 <!--/ address row -->

                 <!-- address row -->
                 <div class="addressrow row">
                    <!-- address col -->
                    <div class="col-md-6">
                       <h4 class="subTitle">Office of Mumbai </h4>
                        <p class="fsbold">Reion Automobiles Private India Limited,</p>
                        <p>Shop no 2, Sheetal Ganga gobind society, Kalina market, Santacruz East Mumbai 500020</p>

                        <article>
                            <small style="color:gray">Phone Number</small>
                            <p>414-214-0362, 515-215-0363</p>
                        </article>                           
                    </div>
                    <!--/ address col -->

                    <!-- map -->
                    <div class="col-md-6">
                       <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3770.670350987479!2d72.8653996!3d19.078226!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c92f5080a78f%3A0xf8089858c3d7b02e!2sGanga%20Govind%20society!5e0!3m2!1sen!2sin!4v1646846884588!5m2!1sen!2sin" width="100%" height="350" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                    <!--/ map -->
                 </div>
                 <!--/ address row -->

                 <!-- address row -->
                 <div class="addressrow row">
                    <!-- address col -->
                    <div class="col-md-6">
                      <h4 class="subTitle">Office of Iran</h4>
                        <p class="fsbold">Reion,</p>
                        <p>4th Floor/Bir buildings, blok B, Allamhe Tabatabai st, Tabriz, IRAN P.O.BOX:5154795659 </p>

                        <article>
                            <small style="color:gray">Phone Number</small>
                            <p>+98 41 33258016 / 33258817 / 33258136 </p>
                        </article>
                            <article>
                            <small style="color:gray">Fax Number</small>
                            <p>+98 41 33258135 </p>
                        </article>                           
                    </div>
                    <!--/ address col -->

                    <!-- map -->
                    <div class="col-md-6">
                      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3140.7627722662473!2d46.316362!3d38.0759055!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x401a1b8d182f4ff3%3A0x78f8ea536ba443e!2z2LTYsdqp2Kog2KjbjNix!5e0!3m2!1sen!2sin!4v1646846980356!5m2!1sen!2sin" width="100%" height="350" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                    <!--/ map -->
                 </div>
                 <!--/ address row -->
             </div>
             <!--/ container -->
        </section>
        <!--/ sub page body -->
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?>
</body>

</html>