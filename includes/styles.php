 <link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
    <!-- style sheets -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/swiper.css">
    <link rel="stylesheet" href="css/bsnav.min.css">
    <link rel="stylesheet" href="css/style.css">