 <!-- scripts -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!--[if lte IE 9]>
        <script src="js/ie.lteIE9.js"></script>
    <![endif]-->
    <script src="js/swiper.min.js"></script>
    <script src="js/html5-shiv.js"></script>
    <script src="js/bsnav.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/custom.js"></script>

     <script>
        //contact form validatin
        $('#contactForm').validate({
            ignore: [],
            errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            errorPlacement: function (error, e) {
                e.parents('.form-floating').append(error);
            },
            highlight: function (e) {
                $(e).closest('.form-floating').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.text-danger').remove();
            },
            rules: {
                name: {
                    required: true
                },
                 email: {
                    required: true,
                    email: true
                },
                phone: {
                    required: true,
                    number: true,
                    minlength: 10,
                    maxlength: 10
                },               
                location: {
                    required: true                    
                },
                subject: {
                    required: true,
                }
            },

            messages: {
                name: {
                    required: "Enter Name"
                },
                email: {
                    required: "Enter Valid Email"
                },
                phone: {
                    required: "Enter Valid Mobile Number"
                },
                location: {
                    required: "Enter Your Location"
                }, 
                subject: {
                    required: "Enter Subject"
                }
            },
        });
    </script>