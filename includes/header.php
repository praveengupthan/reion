 <!-- header -->
 <div id="load"><img src="img/DzUd.gif"></div>   
 <header class="fixed-top">
    <div class="customContainer">
        <div class="navbar navbar-expand-lg bsnav"><a class="navbar-brand" href="index.php">
                <img src="img/logowhite.svg" alt="">
            </a>
            <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse justify-content-md-between">
                <ul class="navbar-nav navbar-mobile mx-auto">
                    <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='index.php'){echo'active';}else {echo'nav-link';}?>" href="index.php">Home</a></li>
                    <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='company.php'){echo'active';}else {echo'nav-link';}?>" href="company.php">Company </a></li>
                    <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='coreactivities.php'){echo'active';}else {echo'nav-link';}?>" href="coreactivities.php">Core Activities</a></li>
                    <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='services.php'){echo'active';}else {echo'nav-link';}?>" href="services.php">Services</a></li>
                    <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='buses.php'){echo'active';}else {echo'nav-link';}?>" href="buses.php">Buses </a> </li>
                    <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='vehicles.php'){echo'active';}else {echo'nav-link';}?>" href="vehicles.php">Vehicles</a></li>
                </ul>
                <ul class="navbar-nav navbar-mobile">
                    <li class="nav-item">
                        <div class="phnumber d-flex">
                            <span class="icon-telephone icomoon d-none d-lg-block"></span>
                            <article>
                                <h3 class="fsbold m-0 p-0">+91 80972 38020</h3>
                                <small>Make a call</small>
                            </article>
                        </div>
                    </li>
                    <li class="nav-item getintouch btn"><a class="nav-link" href="contact.php">Get in Touch</a></li>
                </ul>
            </div>
        </div>
        <div class="bsnav-mobile">
            <div class="bsnav-mobile-overlay"></div>
            <div class="navbar"></div>
        </div>
    </div>
</header>
<!--/ header -->