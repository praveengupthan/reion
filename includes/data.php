<?php 
$homeSliderItem = array(
    array(
        "slider01.jpg",
        "Market Leading Producer of",
        "Electric Busses",
        "As one of the leading manufacturers of premium buses, coaches and bus chassis, we strive to be the ultimate provider of sustainable transport solutions.",
        "buses.php"
    ),
    array(
        "slider02.jpg",
        "This is Producer of",
        "Reion Buses",
        "Safety, comfort and reliability. Our products and services are designed for a first-class passenger experience, ranging from efficient city transport solutions to luxury coach travel. We aim to deliver a premium experience every time.",
        "buses.php"
    ),
     array(
        "slider03.jpg",
        "Premium passenger",
        "experience",
        "As one of the leading manufacturers of premium buses, coaches and bus chassis, we strive to be the ultimate provider of sustainable transport solutions.",
        "vehicles.php"
    ),
);

//home vehicle item
$homeVehicleItem = array(
    array(
        "12mEbusluxury.jpg",
        "12M  Luxury & Standard E Bus",
        "With the Reion 1200L Electri9c bus range you get clean, silent and efficient public transport, a new level of capacity, and our flexible charging concept ensures operation on your current schedule",
        "buses.php"
    ),
    array(
        "airportbus.jpg",
        "Airport Bus",
        "With the Reion 1200L Electri9c bus range you get clean, silent and efficient public transport, a new level of capacity, and our flexible charging concept ensures operation on your current schedule",
        "vehicles.php#AirportBus"
    ),
    array(
        "caravan.jpg",
        "Caravan",
        "With the Reion 1200L Electri9c bus range you get clean, silent and efficient public transport, a new level of capacity, and our flexible charging concept ensures operation on your current schedule",
        "vehicles.php#Caravan"
    ),
    array(
        "medicalvehicle.jpg",
        "Medical Vehicle",
        "We Build a complete Medical Clinic inside a bus, which enables to move around in any area and to provide Medical facilities to people living in Rural and remote locations.",
        "vehicles.php#MedicalVehicle"
    ),
    array(
        "dentalvehicle.jpg",
        "Dental Clinic",
        "Mobile and portable dental services are a viable option to take the sophisticated oral health services to the doorsteps of the underserved population. ",
        "vehicles.php#DentalClinic"
    ),
    array(
        "mobileeyeclinic.jpg",
        "Mobile Eye Clinic",
        "The Mobile Eye Clinic is fully equipped to take care of primary eye care with trained personnel, which can be taken to the remotest places to provide quality eye care to the people Unreached",
        "vehicles.php#MobileEyeClinic"
    ),
    array(
        "mobilepharmacy.jpg",
        "Mobile Pharmacy",
        "Now that the Central Govt. has given in-principal approval to start mobile Pharmacy shops to serve the people not reachable. So with the help of Mobile Pharmacies....",
        "vehicles.php#MobilePharmacy"
    ),
    array(
        "mobilelab.jpg",
        "Mobile laboratory",
        "Provide the best Mobile Laboratory Facilities possible. Being mobile, allows for this system to serve different locations where there is a need for immediate laboratory testing. ",
        "vehicles.php#Mobilelaboratory"
    ),
    array(
        "ambulance.jpg",
        "Ambulance",
        "With the increasing complications as on date, depending on the patient’s condition, immediate Medical Care is required on the way to hospital (Transit). In those cases, institutions prefer the vehicles....",
        "vehicles.php#Ambulance"
    ),
    array(
        "mobileshop.jpg",
        "Mobile shops",
        "The mobile Libraries are moving to different schools and colleges during their working hours and evenings they will be stationed at Community centre’s and Parks to facilitate the elderly people.",
        "vehicles.php#Mobileshops"
    ),
    array(
        "foodtruck.jpg",
        "Food truck",
        "Food Truck is a kind of a restaurant on wheels and an economical alternative to restaurants. A mobile food truck can go to where its customers are. They consist of modern kitchen equipment.",
        "vehicles.php#Foodtruck"
    ),
    array(
        "cargo.jpg",
        "Cargo van",
        "The mobile Libraries are moving to different schools and colleges during their working hours and evenings they will be stationed at Community centre’s and Parks to facilitate the elderly people.",
        "vehicles.php#Cargovan"
    ),

);
?>