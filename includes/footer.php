 <!-- footer -->
    <footer>
        <div class="container topfooter pb-3 mb-3">
            <figure class="text-center border-bottom">
                <a href="javascript:void(0)" class="footerbrand">
                    <img src="img/logo.svg">
                </a>
            </figure>
            <div class="d-lg-flex justify-content-center footernav text-center">
                <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='index.php'){echo'footerlinkActive';}else {echo'footerLink';}?>" href="index.php">Home</a>
                <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='company.php'){echo'footerlinkActive';}else {echo'footerLink';}?>" href="company.php">Company</a>
                <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='coreactivities.php'){echo'footerlinkActive';}else {echo'footerLink';}?>" href="coreactivities.php">Core Activities</a>
                <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='services.php'){echo'footerlinkActive';}else {echo'footerLink';}?>" href="services.php">Services</a>
                <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='buses.php'){echo'footerlinkActive';}else {echo'footerLink';}?>" href="buses.php">Buses</a>
                <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='vehicles.php'){echo'footerlinkActive';}else {echo'footerLink';}?>" href="vehicles.php">Vehicles</a>
                <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='contact.php'){echo'footerlinkActive';}else {echo'footerLink';}?>" href="contact.php">Contact</a>
            </div>
            <div class="d-flex justify-content-center footernav mt-4">
                <a class="footerLink" href="javascript:void(0)"><span class="icon-facebook icomoon"></span></a>
                <a class="footerLink" href="javascript:void(0)"><span class="icon-twitter icomoon"></span></a>
                <a class="footerLink" href="javascript:void(0)"><span class="icon-linkedin icomoon"></span></a>
            </div>
        </div>
        <div class="bottomFooter text-center">
            <p>Reion 2022. All rights reserved</p>
        </div>
        <a id="movetop" class="movetop" href="javascript:void(0)"><span class="icon-downarrow icomoon"></span></a>
    </footer>
    <!--/ footer -->