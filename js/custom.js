//on click move to browser top
$(document).ready(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $("#movetop").fadeIn();
        } else {
            $("#movetop").fadeOut();
        }
    });
});

//click event to scroll to top
$("#movetop").click(function () {
    $("html, body").animate({ scrollTop: 0 }, 200);
});


var swiper = new Swiper('.homeinnerSlider', {
    spaceBetween: 30,
    effect: 'fade',
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    autoplay: {
        delay: 3000,
    },
});

//vehicles 
var swiper = new Swiper('.homeVehicles', {
    slidesPerView: 1,
    spaceBetween: 0,
    autoplay: {
        delay: 5000,
        disableOnInteraction: true,
    },
    // init: false,   
    breakpoints: {
        640: {
            slidesPerView: 2,
            spaceBetween: 0,
        },
        768: {
            slidesPerView: 4,
            spaceBetween: 0,
        },
        1024: {
            slidesPerView: 4,
            spaceBetween: 0,
        },
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});

//header add class
$(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
        $("header").addClass("fixedtheme");
    } else {
        $("header").removeClass("fixedtheme");
    }
});

document.onreadystatechange = function () {
    var state = document.readyState
    if (state == 'complete') {
        setTimeout(function () {
            document.getElementById('load').style.visibility = "hidden";
        }, 1000);
    }
}
