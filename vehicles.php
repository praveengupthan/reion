<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reion Electric Luxury Bus</title>
     <?php include 'includes/styles.php'?>
</head>

<body>
    <?php include 'includes/header.php' ?>
    <!-- main -->
    <main class="subpage">
        <!-- sub page header -->
        <section class="subpageHeader">
             <div class="customContainer">
                 <div class="titleSection">
                     <div class="row">
                         <div class="col-md-6">
                             <div class="sectionTitle">
                                 <p>Categories</p>
                                 <h1 class="p-0 m-0">Vehicles</h1>
                             </div>
                         </div>
                         <div class="col-md-6 align-self-center">
                              <ul class="nav justify-content-end">
                                   <li class="nav-item"><a class="nav-link" href="index.php">Home</a></li>  
                                   <li class="nav-item"><a class="nav-link" href="javascript:void(0)">Vehicles</a></li>
                              </ul>
                         </div>
                     </div>
                 </div>
             </div>
        </section>
        <!--/ sub page header -->
        <!-- sub page body -->
        <section class="subpageBody">
             <!-- container -->
             <div class="customContainer">   
                 
                <!-- row -->
                <div class="row" id="AirportBus" >
                    <div class="col-md-6 order-md-last">
                        <img src="img/airportbusVehicle.jpg" alt="" class="img-fluid w-100">
                    </div>
                    <div class="col-md-6 align-self-center">
                        <div class="sectionTitle">
                            <p>Vehicles</p>
                            <h3>Airport Bus</h3>
                        </div>
                        <p>airports across the country are making the shift to 100% electric bus fleets with . The reion vehicle outperforms other bus technologies in every major performance category — efficiency, acceleration, and passenger-carrying power — with the greatest range of any battery-powered bus on the market. More economical over the lifetime of the vehicle.</p>
                        <p>airports across the country are making the shift to 100% electric bus fleets with . The reion vehicle outperforms other bus technologies in every major performance category — efficiency, acceleration, and passenger-carrying power — with the greatest range of any battery-powered bus on the market. More economical over the lifetime of the vehicle.</p>
                    </div>
                </div>
                <!--/ row -->

                 <!-- row -->
                <div class="row" id="Caravan">
                    <div class="col-md-6">
                        <img src="img/caravan1.jpg" alt="" class="img-fluid w-100">
                    </div>
                    <div class="col-md-6 align-self-center">
                        <div class="sectionTitle">
                            <p>Vehicles</p>
                            <h3>Caravan</h3>
                        </div>                       
                        <p>specialize in manufacturing an exquisite range of Mobile Homes. These homes are designed in compliance with the requirements of our customers using optimum quality raw material and advanced machines and tools. Owing to their splendid colors, aesthetic designs, durability, environment friendliness and waterproofing properties, Our team members design these homes keeping in mind the corporate and residential look.  Specifications/ Facilities in Motor Homes From a spacious yet cozy lounge with a galore of amenities, dining, and elegant decor to bedroom, modern entertainment system, pantry, and washroom; we offer various layouts that are stylish, practical and functional. The vehicle is equipped with tables that can be used for a variety of purposes.</p>
                    </div>
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row" id="MedicalVehicle">
                    <div class="col-md-6 order-md-last">
                        <img src="img/medicalvehicle.jpg" alt="" class="img-fluid w-100">
                    </div>
                    <div class="col-md-6 align-self-center">
                        <div class="sectionTitle">
                            <p>Vehicles</p>
                            <h3>Medical Vehicle</h3>
                        </div>
                        <p>We Build a complete  Medical Clinic inside a bus, which enables to move around  in any area and to provide Medical facilities to people living in Rural and remote locations.  These mobile clinics will be helpful to extend the post operative treatment/ care at regular intervals to those who cannot travel long distances from rural areas to the hospitals/clinics located at far places. </p>
                        <p>Keeping in view of the requirements of the Doctors and Patients, we can build Mobile Clinic Vehicles equipped with latest equipment and can be taken any where required to serve the people. The details of the facilities provided in the Mobile Clinic adhere to the standards specified by the Indian Medical Association.</p>
                    </div>
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row" id="DentalClinic">
                    <div class="col-md-6">
                        <img src="img/dentalclinicvehicle.jpg" alt="" class="img-fluid w-100">
                    </div>
                    <div class="col-md-6 align-self-center">
                        <div class="sectionTitle">
                            <p>Vehicles</p>
                            <h3>Dental Clinic</h3>
                        </div>
                        <p>Mobile and portable dental services are a viable option to take the sophisticated oral health services to the doorsteps of the underserved population. The mobile dental services model offers access to dental care, preventive healthcare, and chronic disease screening and management services. Specific services offered by mobile dental programs may include dental exams, education, and dental sealants.</p>
                        <p>They are the way to reach every section of the community in the absence of national oral health policy and organized school dental health programs in India.  To meet the growing demand in the Mobile Dental Clinics from the Hospitals, Community Centers, Health Care Centers etc., Reion has prepared to take the opportunity of the market demand in providing the Moving Dental Clinics to its Customers.</p>
                    </div>
                </div>
                <!--/ row -->

                 <!-- row -->
                <div class="row" id="MobileEyeClinic">
                    <div class="col-md-6 order-md-last">
                        <img src="img/eyeclinicvehicle.jpg" alt="" class="img-fluid w-100">
                    </div>
                    <div class="col-md-6 align-self-center">
                        <div class="sectionTitle">
                            <p>Vehicles</p>
                            <h3>Mobile Eye Clinic</h3>
                        </div>
                        <p>The Mobile Eye Clinic is fully equipped to take care of primary eye care with trained personnel, which can be taken to the remotest places to provide quality eye care to the people Unreached" .   Our mobile clinics will allow the Doctors to provide high-quality care to underserved populations throughout the area. We can offer full line of platforms to start custom mobile optometry clinic.  </p>
                        <p>We have introduced this Mobile Eye Clinic consisting of all the necessary equipment required for primary eye check-up, treatment and to undertake the operations in the vehicle.  The Mobile Eye Clinic can go to remote places all over.</p>
                    </div>
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row" id="MobilePharmacy">
                    <div class="col-md-6">
                        <img src="img/mobilepharmacyvehicle.jpg" alt="" class="img-fluid w-100">
                    </div>
                    <div class="col-md-6 align-self-center">
                        <div class="sectionTitle">
                            <p>Vehicles</p>
                            <h3>Mobile Pharmacy</h3>
                        </div>
                        <p>Now that the Central Govt. has given in-principal approval to start mobile Pharmacy shops to serve the people not reachable. So with the help of Mobile Pharmacies, NGOs and other donors can distribute the low-cost, but effective, medicines to needy people.</p>
                        <p>The Mobile Pharmacy will have an outdoor patients department, with a doctor, who will examine patients and prescribe generic medicines then and there with the help in house pharmacy set up in the Van. So the requirement of Vehicles built for Mobile Pharmacy is increase day by day. </p>
                    </div>
                </div>
                <!--/ row -->

                 <!-- row -->
                <div class="row" id="Mobilelaboratory">
                    <div class="col-md-6 order-md-last">
                        <img src="img/mobilelab.jpg" alt="" class="img-fluid w-100">
                    </div>
                    <div class="col-md-6 align-self-center">
                        <div class="sectionTitle">
                            <p>Vehicles</p>
                            <h3>Mobile laboratory</h3>
                        </div>
                        <p>Provide the best Mobile Laboratory Facilities possible. Being mobile, allows for this system to serve different locations where there is a need for immediate laboratory testing.  Reion Will maximize the internal space (proving our laboratory customers), with effective and efficient storage, testing, and documentation solutions.  Since each hospital requirement is different, Reion will work directly with them to provide a customized solution.
  </p>
                        <p>Our medical laboratory is specifically designed as a clinical laboratory where tests can be performed (on clinical specimens), in order to get information about the health of patients as pertaining to the diagnosis, treatment, and prevention of diseases. </p>
                    </div>
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row" id="Ambulance">
                    <div class="col-md-6">
                        <img src="img/ambulance1.jpg" alt="" class="img-fluid w-100">
                    </div>
                    <div class="col-md-6 align-self-center">
                        <div class="sectionTitle">
                            <p>Vehicles</p>
                            <h3>Ambulance</h3>
                        </div>
                        <p>With the increasing complications as on date, depending on the patient’s condition, immediate Medical Care is required on the way to hospital (Transit). In those cases, institutions prefer the vehicles with Advanced Life saving system (ALS) built in Ambulance Vehicles.</p>
                        <p>With the facilities in the ALS vehicle, teleconferencing through the tablets helps paramedics treat/diagnose patients on their way to the hospital.    The vehicles have been equipped with Wi-Fi and GPRS Facility to track & Monitor  The audio-visual communication platform, allows ambulance staff to have a video consultation with hospital physicians.</p>
                    </div>
                </div>
                <!--/ row -->

                 <!-- row -->
                <div class="row" id="Mobileshops">
                    <div class="col-md-6 order-md-last">
                        <img src="img/mobileshop.jpg" alt="" class="img-fluid w-100">
                    </div>
                    <div class="col-md-6 align-self-center">
                        <div class="sectionTitle">
                            <p>Vehicles</p>
                            <h3>Mobile Library</h3>
                        </div>
                        <p>The mobile Libraries are moving to different schools and colleges during their working hours and evenings they will be stationed at Community centre’s and Parks to facilitate the elderly people.   Keeping in view of the Present & Future Mobile Library Vehicle requirements, Reion has planned to offer the vehicles with embedded high end facilities.  </p>
                        <p>MMobile Library now a days is an increasing trend in the market which can be moved to different  places for giving facilities to people and increase business in areas where it is difficult to reach </p>
                    </div>
                </div>
                <!--/ row -->

                 <!-- row -->
                <div class="row" id="Foodtruck">
                    <div class="col-md-6">
                        <img src="img/foodtruck.jpg" alt="" class="img-fluid w-100">
                    </div>
                    <div class="col-md-6 align-self-center">
                        <div class="sectionTitle">
                            <p>Vehicles</p>
                            <h3>Food truck</h3>
                        </div>
                        <p>Food Truck is a kind of a restaurant on wheels and an economical alternative to restaurants.  A mobile food truck can go to where its customers are.   They consist of modern kitchen equipment.  They have their own sitting area or a table to have food.</p>
                        <p>These food trucks are equipped with storage space as well to store the extra cooking material.   Entertainment and lighting and other electronics are equipped in the vehicle as per customer design.  Decoration of the vehicle is done in house according to our clients demand. </p>
                    </div>
                </div>
                <!--/ row -->

                  <!-- row -->
                <div class="row pb-md-5" id="Cargovan">
                    <div class="col-md-6 order-md-last">
                        <img src="img/cargo.jpg" alt="" class="img-fluid w-100">
                    </div>
                    <div class="col-md-6 align-self-center">
                        <div class="sectionTitle">
                            <p>Vehicles</p>
                            <h3>Cargo van</h3>
                        </div>
                        <p>The mobile Libraries are moving to different schools and colleges during their working hours and evenings they will be stationed at Community centre’s and Parks to facilitate the elderly people.   Keeping in view of the Present & Future Mobile Library Vehicle requirements, Reion has planned to offer the vehicles with embedded high end facilities.  </p>
                        <p>MMobile Library now a days is an increasing trend in the market which can be moved to different  places for giving facilities to people and increase business in areas where it is difficult to reach </p>
                    </div>
                </div>
                <!--/ row -->
             </div>
             <!--/ container -->
        </section>
        <!--/ sub page body -->
    </main>
    <!--/ main -->
    <?php include 'includes/footer.php' ?>
    <?php include 'includes/scripts.php' ?>
</body>

</html>